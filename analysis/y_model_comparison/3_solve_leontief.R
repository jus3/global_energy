#####################
# Solve HIO and DIO #
#####################
rm(list = ls())

wd <- "~/global_energy"
setwd(wd)
source(paste0(wd, "/derived/0_globals.R"))

raw <- "/data/jus3/GlobalIO/raw"
raw.eora <- paste0("/data/jus3/GlobalIO/raw/EORA/", eora.version)
input <- "/data/jus3/GlobalIO/output/analysis/model_comparison"
output <- "/data/jus3/GlobalIO/output/analysis/model_comparison"
temp <- "/data/jus3/GlobalIO/temp"
figures <- "/data/jus3/GlobalIO/output/figures"

library('magrittr')
library('dplyr')
library('hiofunctions')

# Set year of analysis
in.year <- 2007

country <- 'USA'
year <- in.year
#########################
# Import DIO
assign('dio.T', readRDS(file.path(output, paste0('DIO_T_', country, '_', year, '.rds'))))
assign('dio.FD', readRDS(file.path(output, paste0('DIO_FD_', country, '_', year, '.rds'))))
assign('dio.Q', readRDS(file.path(output, paste0('DIO_Q_', country, '_', year, '.rds'))))
assign('dio.x', readRDS(file.path(output, paste0('DIO_x_', country, '_', year, '.rds'))))

# Import HIO
assign('hio.T', readRDS(file.path(output, paste0('HIO_T_', country, '_', year, '.rds'))))
assign('hio.FD', readRDS(file.path(output, paste0('HIO_FD_', country, '_', year, '.rds'))))
assign('hio.x', readRDS(file.path(output, paste0('HIO_x_', country, '_', year, '.rds'))))

#############
# Solve DIO #
#############
# Assign A
assign('dio.A', t(t(dio.T)/dio.x))

# Assign identiy matrix
assign('dio.I', diag(nrow = nrow(dio.A), ncol = ncol(dio.A)))

# Solve for L
assign('dio.I_A', dio.I-dio.A)
assign('dio.L', solve(dio.I_A))

# Solve for energy intensity:
assign('dio.x_diag', diag(1/dio.x))
assign('dio.intensity', dio.Q%*%dio.x_diag)

# Solve for total energy requirements
assign('dio.L_energy', dio.intensity%*%dio.L)

# Obtain total energy consumption
dio.FD <- rowSums(dio.FD)
assign('dio.energy', dio.L_energy%*%dio.FD)

#############
# Solve HIO #
#############
# Assign A
assign('hio.A', t(t(hio.T)/hio.x))
assert('hio.A < 1')

# Assign identiy matrix
assign('hio.I', diag(nrow = nrow(hio.A), ncol = ncol(hio.A)))

# Solve for L
assign('hio.I_A', hio.I-hio.A)
assign('hio.L', solve(hio.I_A))

# Calculate total energy use: Lf = x
hio.FD <- rowSums(hio.FD)
assign('hio.energy', hio.L%*%hio.FD)



