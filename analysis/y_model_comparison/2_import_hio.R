#####################################################
# Separate US domestic transactions to run Eora-DIO #
#####################################################
rm(list = ls())

wd <- "~/global_energy"
setwd(wd)
source(paste0(wd, "/derived/0_globals.R"))

raw <- "/data/jus3/GlobalIO/raw"
raw.eora <- paste0("/data/jus3/GlobalIO/raw/EORA/", eora.version)
input <- "/data/jus3/GlobalIO/output/derived"
output <- "/data/jus3/GlobalIO/output/analysis/model_comparison"
temp <- "/data/jus3/GlobalIO/temp"
figures <- "/data/jus3/GlobalIO/output/figures"

library('magrittr')
library('dplyr')
library('hiofunctions')

# Set year of analysis
in.year <- 2007

#sink('~/global_energy/analysis/y_model_comparison/out/2_import_hio.txt')
#####################################################
# Import HIO tables
import_hybrid_eora(year = in.year)

# Subset to domestic transactions
domestic.subset <- function(country, year) {

  assign('T', hybrid.T)
  assign('FD', hybrid.FD)

  environment(assert) <- environment()

  # Subset T to domestic T
  assign('c.T', T[grepl(country, rownames(T)), grepl(country, colnames(T))])
  assert('nrow(c.T) == 39 & ncol(c.T) == 39')

  # Subset FD to domestic FD
  assign('c.FD', FD[grepl(country, rownames(FD)), grepl(country, colnames(FD))])
  assert('nrow(c.FD) == 39 & ncol(c.FD) == 6')

  # Calculate total output (x)
  assign('x_int', colSums(c.T))
  assign('x_fd', rowSums(c.FD))
  assign('c.x', x_int + x_fd)

  saveRDS(c.T, file.path(output, paste0('HIO_T_', country, '_', year, '.rds')))
  saveRDS(c.FD, file.path(output, paste0('HIO_FD_', country, '_', year, '.rds')))
  saveRDS(c.x, file.path(output, paste0('HIO_x_', country, '_', year, '.rds')))
}

# Run programs
domestic.subset('USA', in.year)





