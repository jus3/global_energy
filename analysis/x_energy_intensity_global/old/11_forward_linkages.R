###########################################
# Plot energy intensity by origin country #
###########################################
library('RColorBrewer')

# Import data #
###############
import_df <- function(year, model) {
  
  df <- readRDS(file.path(input, paste0(model, '_longform/long_', year, '.rds')))

  df <- subset(df, from.country != 'BLR')
  
  df <- subset(df, from.country != to.country)
  
  df <- group_by(df, from.country, from.sector, year) %>% summarize(value = sum(value)) 
  
  return(df)
  
}

# Run program
hiodf <- data.frame()
diodf <- data.frame()

for (y in 1995:2015) {
  print('####################')
  print(paste0('YEAR = ', y))
  print('####################')
  
  print('Importing DIO')
  df <- import_df(year = y, model = 'DIO')
  names(df)[names(df) == 'value'] <- 'DIO_value'
  diodf <- rbind(as.data.frame(diodf), as.data.frame(df))
  
  print('Importing HIO')
  df <- import_df(year = y, model = 'HIO')
  names(df)[names(df) == 'value'] <- 'HIO_value'
  hiodf <- rbind(as.data.frame(hiodf), as.data.frame(df))
}

# Combine models
basedf <- full_join(diodf, hiodf, by = c('from.country', 'from.sector', 'year'))
basedf$DIO_value[is.na(basedf$DIO_value)] <- basedf$HIO_value[is.na(basedf$HIO_value)] <- 0

# Calculate difference between direct and total
basedf$EMB_value <- basedf$HIO_value-basedf$DIO_value

# Keep top 5 countries by year
diodf <- hiodf <- plotdf

diodf <- arrange(diodf, year, desc(DIO_value)) %>%
         group_by(year) %>%
         mutate(n = row_number())

hiodf <- arrange(hiodf, year, desc(HIO_value)) %>%
         group_by(year) %>%
         mutate(n = row_number())

diodf <- subset(diodf, n < 6)
hiodf <- subset(hiodf, n < 6)

# Add total_output
import_fd_x <- function(year) {
  
  import_hybrid_eora(year = year, balance_flows = F)
  
  x <- as.data.frame(hybrid.x)
  x$sector <- substr(rownames(x), 5, nchar(rownames(x)))
  x$country <- substr(rownames(x), 1, 3)
  names(x) <- c('output', 'sector', 'country')
  x$year <- year
  
  return(x)
}

total_output <- data.frame()

for (y in 1995:2015) {
  df <- import_fd_x(y)
  total_output <- rbind(as.data.frame(total_output), as.data.frame(df))
}

basedf <- left_join(basedf, total_output, by = c('to.sector' = 'sector', 'to.country' = 'country', 'year'))
