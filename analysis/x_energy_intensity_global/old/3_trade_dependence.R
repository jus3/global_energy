############################
# Extract trade dependence #
############################
# Plot trade dependence over time
trade_dependence <- function(model, importer, by_energy = FALSE) {
  
  print("Building dataset")
  plotdf <- import_combine(model = model, importer = importer) 
  
  plotdf <- subset(plotdf, grepl('ELEC_', from.sector) == F)
  
  if (by_energy == FALSE) {
    
    plotdf <-
      collapse_df(df = plotdf, collapse_by = 'region') %>%
      aggregate_sectors() %>%
      pad_zero()
    
  } else {
    
    outdf <- data.frame()
    
    # Calculate dependence by energy type
    for (e in c('BIO', 'COAL', 'CRU', 'NG', 'PET', 'NUC')) {
      print(paste0('Adding energy: ', e))
      edf <- subset(plotdf, from.sector == e)
      if (nrow(edf) > 0) {
        edf <- 
          collapse_df(df = edf, collapse_by = 'region') %>%
          aggregate_sectors() %>%
          pad_zero()
        edf$energy <- e
        outdf <- rbind(outdf, edf)
      }
    }
    
    plotdf <- outdf
    
  }
  
  plotdf <- subset(plotdf, flow == 'Imported')
  
  plot <- 
    ggplot(data = plotdf,
           aes(x = year, y = share_imports*100, fill = from.region)) +
    geom_area(colour = 'black') +
    labs(title = paste0('Model: ', model, ', Country: ', importer),
         x = '', y = 'Import dependence (%)', fill = 'Region') + 
    theme(text = element_text(size = 18, family = 'Helvetica-Narrow'),
          axis.text.x = element_text(angle = 90),
          legend.position = 'bottom')
  
  if (by_energy == FALSE) {
    plot <- plot + facet_grid(.~industry)
  } else {
    plot <- plot + facet_grid(vars(energy), vars(industry))
  }
  
  ggsave(plot = plot,
         filename = file.path(figures, paste0('dependence_', model, '_', importer, '.jpg')), 
         device = 'jpg', width = 12, height = 7, units = 'in')
  return(plot)
}

################
# Run programs #
################
# Direct dependence
trade_dependence(model = 'DIO', importer = 'USA')
#trade_dependence(model = 'DIO', importer = 'DEU')
trade_dependence(model = 'DIO', importer = 'CHN')

# Total dependence
trade_dependence(model = 'HIO', importer = 'USA')
#trade_dependence(model = 'HIO', importer = 'DEU')
trade_dependence(model = 'HIO', importer = 'CHN')

