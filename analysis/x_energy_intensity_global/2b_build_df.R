##############################
# Build base DF for plotting #
##############################
library('RColorBrewer')

# Import data #
############### 
import_df <- function(year, model) {
  
  df <- readRDS(file.path(input, paste0(model, '_longform/long_', year, '.rds')))
  
  df$foreignenergy <- 0
  df$foreignenergy[df$to.country != df$from.country] <- 1
  
  df <- left_join(df, regions, by = c('from.country' = 'iso'))
  
  df <- group_by(df, to.country, from.sector, to.sector, foreignenergy, from.region, year) %>% 
        summarize(value = sum(value)) 
  
  df <- aggregate_sectors(df)
  
  return(df)
  
}

# Run program
hiodf <- data.frame()
diodf <- data.frame()

for (y in 1995:2015) {
  print('####################')
  print(paste0('YEAR = ', y))
  print('####################')
  
  print('Importing DirectHIO')
  df <- import_df(year = y, model = 'HIOA')
  names(df)[names(df) == 'value'] <- 'DIO_value'
  diodf <- rbind(as.data.frame(diodf), as.data.frame(df))
  
  print('Importing HIO')
  df <- import_df(year = y, model = 'HIO')
  names(df)[names(df) == 'value'] <- 'HIO_value'
  hiodf <- rbind(as.data.frame(hiodf), as.data.frame(df))
}

# Combine models
basedf <- full_join(diodf, hiodf, by = c('to.country', 'from.sector', 'to.sector.name', 
                                         'industry', 'foreignenergy', 'to.sector', 'year', 'from.region'))
basedf$DIO_value[is.na(basedf$DIO_value)] <- basedf$HIO_value[is.na(basedf$HIO_value)] <- 0

# Add regions
basedf <- left_join(basedf, regions, by = c('to.country' = 'iso'))
names(basedf)[names(basedf) == 'from.region.y'] <- 'to.region'
names(basedf)[names(basedf) == 'from.region.x'] <- 'from.region'

basedf <- subset(basedf, to.country != 'PRK')
basedf$to.region[basedf$to.country == 'USA' | basedf$to.country == 'CAN'] <- 'North America'
basedf$to.region[basedf$to.region == 'North and Central America'] <- 'Central America'

# Add GDP
gdp <- read.csv(file.path(raw, 'WorldBank/WorldBank_GDP_PerCapita.csv'), stringsAsFactors = F)
gdp <- gdp[c('country', 'gdp_percapita', 'year')]

basedf <- left_join(basedf, gdp, by = c('to.country' = 'country', 'year'))

# Calculate difference between direct and total
basedf$EMB_value <- basedf$HIO_value-basedf$DIO_value

# Add intermediate and final demand vectors
final_demand <- data.frame()
int_demand <- data.frame()

for (y in 1995:2015) {
  inlist <- import_fd_x(y)
  fdf <- inlist[[1]]
  xdf <- inlist[[2]]
  final_demand <- rbind(as.data.frame(final_demand), as.data.frame(fdf))
  int_demand <- rbind(as.data.frame(total_output), as.data.frame(xdf))
}

basedf <- left_join(basedf, final_demand, 
                    by = c('to.sector' = 'sector', 'to.country' = 'country', 'year'))
basedf <- left_join(basedf, int_demand,
                    by = c('to.sector' = 'sector', 'to.country' = 'country', 'year'))

saveRDS(basedf, file.path(output, 'plot_basedf.rds'))
