##################################################
# Energy footprint by country, export to Tableau #
##################################################
library('RColorBrewer')

# Build dataframe to export as csv
outdf <- data.frame()

for (c in unique(basedf$to.country)) {

  print(paste0('Compiling...', c))
  
  plotdf <- basedf[c('from.sector', 'to.country', 'to.region', 'foreignenergy',
                     'to.sector', 'to.sector.name', 'industry', 'year', 
                     'DIO_value', 'EMB_value', 'HIO_value', paste0(c, '_f'))]

  names(plotdf)[names(plotdf) == paste0(c, '_f')] <- 'final_demand'
  
  plotdf$imported <- 'imported'
  plotdf$imported[plotdf$to.country == c] <- 'domestic'
  
  plotdf$energysource <- 'foreign'
  plotdf$energysource[plotdf$foreignenergy == 0] <- 'domestic'

  plotdf$direct_energy <- plotdf$DIO_value * plotdf$final_demand
  plotdf$embodied_energy <- plotdf$EMB_value * plotdf$final_demand
  plotdf$total_energy <- plotdf$HIO_value * plotdf$final_demand

  plotdf <- group_by(plotdf, year, imported, energysource) %>%
    summarize(direct_energy = sum(direct_energy, na.rm = T),
              embodied_energy = sum(embodied_energy, na.rm = T),
              total_energy = sum(total_energy, na.rm = T))

  pdf_m <- plotdf[c('year', 'imported', 'energysource', 'embodied_energy')]
  pdf_m$energy <- 'Embodied'
  pdf_d <- plotdf[c('year', 'imported', 'energysource', 'direct_energy')]
  pdf_d$energy <- 'Direct'
  names(pdf_m) <- names(pdf_d) <- c('year', 'imported', 'energysource', 'value', 'energy')
  plotdf <- rbind(pdf_m, pdf_d)
  
  plotdf$country <- c
  
  outdf <- rbind(as.data.frame(outdf), as.data.frame(plotdf))
}

outdf <- group_by(outdf, year, country, imported) %>% mutate(total_energy = sum(value))

outdf$share <- outdf$value/outdf$total_energy
outdf$total_energy <- NULL

write.csv(outdf, file.path(output, 'EF_byCountry_forTableau.csv'))

