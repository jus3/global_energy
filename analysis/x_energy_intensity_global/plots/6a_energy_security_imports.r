##########################################
# Calculate HHI and plot (compare years) #
##########################################
rm(list = ls())

wd <- "~/global_energy"
setwd(wd)

source(paste0(wd, "/derived/0_globals.R"))
source(paste0(wd, "/analysis/x_energy_intensity_global/2a_build_functions.R"))

input <- '/data/jus3/GlobalIO/output/analysis/'
output <- '/data/jus3/GlobalIO/output/analysis/'
raw <- '/data/jus3/GlobalIO/raw/'

#sink(paste0(wd, "/analysis/x_energy_intensity_global/out/5_network_maps.txt"))

# Import cdf
cdf <- readRDS(file.path(input, 'plots/cdf.rds'))

# Change country and energy names
web_countries <- read.csv(file.path(raw, 'ConversionTables/web_countries.csv'), stringsAsFactors = F)
web_countries <- web_countries[c('web.country', 'iso.country')]
web_countries <- group_by(web_countries, iso.country) %>% mutate(n = row_number())
web_countries <- subset(web_countries, n == 1)
web_countries$n <- NULL

cdf <- inner_join(cdf, web_countries, by = c('from.country' = 'iso.country'))
cdf$from.country <- cdf$web.country
cdf$web.country <- NULL

cdf <- inner_join(cdf, web_countries, by = c('to.country' = 'iso.country'))
cdf$to.country <- cdf$web.country
cdf$web.country <- NULL

cdf <- subset(cdf, energy %in% c('BIO', 'CRU', 'COAL', 'NG', 'RE', 'NUC', 'HYD')) # Primary energy only

cdf$energy[cdf$energy == 'BIO'] <- 'Bioenergy'
cdf$energy[cdf$energy == 'CRU'] <- 'Crude'
cdf$energy[cdf$energy == 'COAL'] <- 'Coal'
cdf$energy[cdf$energy == 'NG'] <- 'Natural Gas'
cdf$energy[cdf$energy == 'RE'] <- 'Renewables'
cdf$energy[cdf$energy == 'NUC'] <- 'Uranium'
cdf$energy[cdf$energy == 'HYD'] <- 'Hydro'

cdf <- subset(cdf, from.country != to.country) # Only imports

# Dataframe of top 5 exporters per country
top5df <- subset(cdf, hio_total_energy > 1)
top5df <- arrange(top5df, year, to.country, desc(hio_total_energy))
top5df <- group_by(top5df, year, to.country) %>% mutate(rank = row_number())
top5df <- subset(top5df, rank < 6)

top5df$hio_total_energy <- top5df$hio_total_energy * (10^-6) # to EJ

rankdf <- unique(top5df[c('to.country', 'year')])
rankdf$list <- ''

for (r in 1:3) {
  rdf <- subset(top5df, rank == r)
  rankdf <- left_join(rankdf, rdf, by = c('to.country', 'year'))
  rankdf$list <- paste0(rankdf$list, rankdf$from.country, ' (', rankdf$energy, ', ', 
                        round(rankdf$hio_total_energy, 3), ' EJ), ')
  rankdf <- rankdf[c('to.country', 'year', 'list')]
}

# Dataframe of HHI and partners by country and year
hhidf <- subset(cdf, hio_total_energy > 5)
hhidf <- group_by(hhidf, year, to.country) %>% mutate(total_energy = sum(hio_total_energy))
hhidf$hhi <- (hhidf$hio_total_energy/hhidf$total_energy)*100
hhidf$hhi <- hhidf$hhi^2

hhidf$n <- 1

hhidf <- group_by(hhidf, year, to.country) %>% summarize(hhi = sum(hhi, na.rm = T), n = sum(n, na.rm = T))

# Combine
indf <- inner_join(rankdf, hhidf, by = c('to.country', 'year'))
alldf <- data.frame()

for (y in 1995:2015) {
  tdf <- subset(indf, year == y)
  tdf <- tdf[c('to.country', 'list', 'hhi', 'n')]
  names(tdf) <- c('to.country', paste0('list_', y), paste0('hhi_', y), paste0('nlinks_', y))
  tdf$year <- y
  names(tdf) <- c('to.country', 'list', 'hhi', 'nlinks', 'year')
  alldf <- rbind(as.data.frame(alldf), as.data.frame(tdf))
}

# Add share to cdf
cdf <- group_by(cdf, year, to.country) %>% mutate(total_energy = sum(hio_total_energy))
cdf$share <- cdf$hio_total_energy/cdf$total_energy

# Add energy consumption
efpdf <- group_by(cdf, year, to.country) %>% summarize(total_energy = sum(hio_total_energy, na.rm = T))

# Add import dependence
import_dependence <- readRDS(file.path(output, 'plots/import_dependence.rds'))

direct_ID <- import_dependence[c('year', 'hioa_country', 'hioa_share')]
total_ID <- import_dependence[c('year', 'hio_country', 'hio_share')]

names(direct_ID) <- c('year', 'country', 'direct_ID')
names(total_ID) <- c('year', 'country', 'embodied_ID')

alldf <- inner_join(alldf, direct_ID, by = c('to.country' = 'country', 'year'))
alldf <- inner_join(alldf, total_ID, by = c('to.country' = 'country', 'year'))
alldf <- inner_join(alldf, efpdf, by = c('to.country', 'year'))

alldf <- arrange(alldf, year, desc(total_energy))

alldf$direct_ID <- round(alldf$direct_ID * 100, 2)
alldf$embodied_ID <- round(alldf$embodied_ID * 100, 2)
alldf$total_energy <- alldf$total_energy * (10^-6) # In EJ

# Remove duplicates
alldf <- group_by(alldf, year, hhi, nlinks, direct_ID, embodied_ID, total_energy) %>% mutate(n = row_number())
alldf <- subset(alldf, n == 1)
alldf$n <- NULL

# Variance covariance matrix
inmat <- alldf[c('hhi', 'nlinks', 'direct_ID', 'embodied_ID', 'total_energy')]
inmat <- as.matrix(inmat)
rownames(inmat) <- alldf$to.country

cormat <- cor(inmat)

# Write files
df_2015 <- subset(alldf, year == 2015)
write.csv(df_2015, file.path(output, 'plots/energy_security_2015_IMPORTS.csv'))
write.csv(cdf, file.path(output, 'plots/energy_security_IMPORTS.csv'))
write.csv(cormat, file.path(output, 'plots/correlation_energy_security_IMPORTS.csv'))

# Russia's HHI is high; how much do FSR countries provide?
russia <- subset(cdf, to.country == 'Russian Federation' & year == 2015)

russia <- group_by(russia, from.country) %>% summarize(direct = sum(hioa_total_energy, na.rm = T),
                                                       embodied = sum(hio_total_energy, na.rm = T))
russia$total <- russia$direct + russia$embodied

russia$total_2015 <- sum(russia$total)
russia$share <- russia$total/russia$total_2015

russia <- arrange(russia, desc(share))





