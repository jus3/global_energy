######################################
# Set up chokepoint transit matrices #
######################################
rm(list = ls())

wd <- "~/global_energy"
setwd(wd)
source(paste0(wd, "/derived/0_globals.R"))

raw <- "/data/jus3/GlobalIO/raw"
raw.eora <- paste0("/data/jus3/GlobalIO/raw/EORA/", eora.version)
input <- "/data/jus3/GlobalIO/output/derived"
output <- "/data/jus3/GlobalIO/output/analysis/CP_mat"
temp <- "/data/jus3/GlobalIO/temp"
figures <- "/data/jus3/GlobalIO/output/figures"

library('tidyr')
library('magrittr')
library('dplyr')
library('hiofunctions')

sink('~/global_energy/analysis/z_chokepoints/out/1_chokepoints_matrix.txt')

#############################
print('Import chokpoints transit data')
cp <- read.csv(file.path(raw, "Chokepoints/port_pathmap.csv"), stringsAsFactors = F)
  cp <- unique(cp[c('country1', 'country2', 'fury.and.hecla', 'nares', 'belle.isle', 'gibraltar',
                    'irish.sea', 'kattegat', 'bosporus', 'white.sea', 'bab.el.mandeb', 'hormuz',
                    'malacca', 'soya.kaikyo', 'panama.canal', 'suez.canal', 'cape.horn', 'cape.of.good.hope',
                    'danish.straits', 'english.strait', 'east.china.sea', 'south.china.sea')])

print('Adjust country names')
cnames <- read.csv(file.path(raw, "ConversionTables/port_countrynames.csv"), stringsAsFactors = F)
  cp <- left_join(cp, cnames, by = c('country1' = 'port_country'))
  cp$country1 <- cp$web_country
  cp$web_country <- NULL
  cp <- left_join(cp, cnames, by = c('country2' = 'port_country'))
  cp$country2 <- cp$web_country
  cp$web_country <- NULL

print('Import web.countries')
web.countries <- read.csv(file.path(raw, "ConversionTables/web_countries.csv"), stringsAsFactors = F, strip.white = T)
  web.countries <- unique(web.countries[c('web.country', 'iso.country')])
  web.countries$web.country[web.countries$web.country == "People's Republic of China"] <- "China"
cp <- inner_join(cp, web.countries, by = c('country1' = 'web.country'))
names(cp)[names(cp) == 'iso.country'] <- 'iso.i'
cp <- inner_join(cp, web.countries, by = c('country2' = 'web.country'))
names(cp)[names(cp) == 'iso.country'] <- 'iso.j'

print('Subset to full country list')
cp.countries <- unique(inner_join(cp['iso.i'], cp['iso.j'], by = c('iso.i' = 'iso.j')))

print('Import Eora and subset chokepoint data to Eora (and vice versa)')

# Function : Import eora and calibrate
calibrate_eora <- function(in.year) {
  
  environment(import_eora) <- environment()
  
  import_eora(year = in.year)

  eora.countries <- unique(eora.T.labs[c('country.code')])

  full.countries <- inner_join(cp.countries, eora.countries, by = c('iso.i' = 'country.code'))
  full.countries <- unique(full.countries$iso.i)
  saveRDS(full.countries, file.path(output, paste0('country_list/', in.year, '.rds')))
  assign('full.countries', full.countries, envir = parent.frame())
  
  cp <- subset(cp, iso.i %in% full.countries & iso.j %in% full.countries)

  eora.T <- eora.T[substr(rownames(eora.T), 1, 3) %in% full.countries,
                   substr(colnames(eora.T), 1, 3) %in% full.countries]
  saveRDS(eora.T, file.path(output, paste0('T_mat/', in.year, '/eora_T', '.rds')))
  assign('eora.T', eora.T, envir = parent.frame())
  
  eora.FD <- FD[substr(rownames(FD), 1, 3) %in% full.countries,
                substr(colnames(FD), 1, 3) %in% full.countries]
  saveRDS(eora.FD, file.path(output, paste0('T_mat/', in.year, '/eora_FD', '.rds')))
  assign('eora.FD', eora.FD, envir = parent.frame())
  
  eora.x <- colSums(eora.T) + rowSums(eora.FD)
  saveRDS(eora.x, file.path(output, paste0('T_mat/', in.year, '/eora_x', '.rds')))
  assign('eora.x', eora.x, envir = parent.frame())
  
}

# Function: For each strait, build matrix of transit
cp_matrix <- function(strait.name, in.year) {
  
  environment(isid) <- environment()
  
  assign('mat', base_mat)
  
  # Fill in for chokepoint
  assign('cp_df', cp[c('iso.i', 'iso.j', strait.name)])
  names(cp_df)[names(cp_df) == strait.name] <- 'strait'
  cp_df <- subset(cp_df, strait == 1)
  cp_df <- subset(cp_df, iso.i != iso.j)
  
  if (nrow(cp_df) != 0) {
    
    print(paste0("STRAIT = ", strait.name))
    
    cp_df2 <- cp_df
    names(cp_df2) <- c('iso.j', 'iso.i', 'strait')
    cp_df2 <- cp_df2[c('iso.i', 'iso.j', 'strait')]
    
    cp_df <- rbind(as.data.frame(cp_df), as.data.frame(cp_df2))
    cp_df <- unique(cp_df[c('iso.i', 'iso.j', 'strait')])
    
    isid('cp_df', c('iso.i', 'iso.j'))
    print(paste0("Number of CP transit routes = ", nrow(cp_df)))
    
    for (i in 1:nrow(cp_df)) {
      if (i%%100==0) {
        print(paste0('...route ', i))
      }
      mat[grepl(cp_df$iso.i[i], rownames(mat)), grepl(cp_df$iso.j[i], colnames(mat))] <- cp_df$strait[i]
    }
  } 
  
  assign(paste0('CP.', strait.name), mat, envir = parent.frame())
  name.out <- stringr::str_replace_all(strait.name, "\\.", "_")
  
  saveRDS(mat, file.path(output, paste0('C_mat/', in.year, '/', name.out, '.rds')))
}

for (y in 1995:2015) {
  environment(calibrate_eora) <- environment(cp_matrix) <- environment()
  
  print(paste0('...calibrating Eora, t = ', y))
  calibrate_eora(y)
  
  print('...build base matrix (empty)')
  assign('base_mat', matrix(0, nrow = length(full.countries)*26, ncol = length(full.countries)*26))
  rownames(base_mat) <- rownames(eora.T)
  colnames(base_mat) <- colnames(eora.T)
  assert('rownames(base_mat) == colnames(base_mat)')
  assert('rownames(eora.T) == colnames(eora.T)')
  
  print('...build strait matrices')
  for (c in chokepoint_list) {
    print(paste0('...', c))
    cp_matrix(c, y)
  }
}
sink()
