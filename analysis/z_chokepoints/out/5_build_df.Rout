
R version 3.6.0 (2019-04-26) -- "Planting of a Tree"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-redhat-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> ##################
> # Compile output #
> ##################
> rm(list = ls())
> 
> wd <- "~/global_energy"
> setwd(wd)
> source(paste0(wd, "/derived/0_globals.R"))
> 
> raw <- "/data/jus3/GlobalIO/raw"
> input <- "/data/jus3/GlobalIO/output/analysis/CP_mat"
> output <- "/data/jus3/GlobalIO/output/analysis/CP_mat/"
> temp <- "/data/jus3/GlobalIO/temp"
> figures <- "/data/jus3/GlobalIO/output/figures"
> 
> library('tidyr')
> library('magrittr')

Attaching package: ‘magrittr’

The following object is masked from ‘package:tidyr’:

    extract

> library('dplyr')

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union

> library('hiofunctions')
> library('reshape2')

Attaching package: ‘reshape2’

The following object is masked from ‘package:tidyr’:

    smiths

> 
> sink('~/global_energy/analysis/z_chokepoints/out/5_build_total.txt')
> 
> # Function: import cp matrices
> import_cp_files <- function(strait.name, in.year) {
+   
+   assign('out.name', stringr::str_replace_all(strait.name, "\\.", "_"))
+   
+   assign('Lcp', readRDS(file.path(input, paste0('L_mat/', in.year, '/Lcp_', out.name, '.rds'))), envir = parent.frame())
+   assign('Acp', readRDS(file.path(input, paste0('A_mat/', in.year, '/Acp_', out.name, '.rds'))), envir = parent.frame())
+ }
> 
> # Function: collapse by final demand
> collapse_FD <- function(inmat, inmat_name) {
+   FDc <- FDc[colnames(inmat)]
+   assign('outvec', inmat%*%FDc)
+   outvec <- as.data.frame(outvec)
+   outvec$from <- rownames(outvec)
+   names(outvec) <- c(paste0(inmat_name, '_value'), 'from')
+   return(outvec)
+ }
> 
> # Get total amounts through each chokepoint
> combine_all <- function(in.year) {
+   
+   environment(import_cp_files) <- environment(collapse_FD) <- environment()
+   
+   assign('L', readRDS(file.path(input, paste0('L_mat/', in.year, '/L', '.rds'))))
+   assign('A', readRDS(file.path(input, paste0('A_mat/', in.year, '/A', '.rds'))))
+   assign('FD', readRDS(file.path(input, paste0('T_mat/', in.year, '/eora_FD.rds'))))
+   
+   # List of countries in FD
+   assign('country_list', unique(substr(colnames(FD), 1, 3)))
+   
+   # Run by country
+   assign('outdf', data.frame())
+   
+   for (country in country_list) {
+     
+     print(paste0('...Compiling ', country))
+     
+     # Collapse final demand
+     assign('FDc', FD[,grepl(country, colnames(FD))])
+     FDc <- rowSums(FDc)
+ 
+     # Link to L and A (overall)
+     assign('Lf', collapse_FD(L, 'L'))
+     assign('Af', collapse_FD(A, 'A'))
+     
+     assign('all_LA', full_join(Lf, Af, by = c('from')))
+     all_LA$chokepoint <- 'Overall'
+     
+     for (cp in chokepoint_list) {
+       
+       assign('out.name', stringr::str_replace_all(cp, "\\.", "_"))
+       
+       if (file.exists(file.path(input, paste0('A_mat/',in.year, '/Acp_', out.name, '.rds')))) {
+         print(paste0('Compiling ', cp))
+         import_cp_files(cp, in.year)
+         
+         # Solve for L and A for chokepoint
+         assign('Lcpf', collapse_FD(Lcp, 'L'))
+         assign('Acpf', collapse_FD(Acp, 'A'))
+         
+         assign('cp_LA', full_join(Lcpf, Acpf, by = c('from')))
+         
+         cp_LA <- cp_LA[c('from', 'L_value', 'A_value')]
+         cp_LA$chokepoint <- out.name
+         all_LA <- rbind(as.data.frame(all_LA), as.data.frame(cp_LA))
+       }
+     }
+     all_LA$country <- country
+     
+     outdf <- rbind(as.data.frame(outdf), as.data.frame(all_LA))
+   }
+   
+   outdf$year <- in.year
+   
+   return(outdf)
+ }
> 
> # Run by year
> for (y in 1995:2015) {
+   
+   print(paste0('###### YEAR = ', y, ' ######'))
+   
+   assign('yeardf', combine_all(y))
+   
+   saveRDS(yeardf, file.path(output, paste0('total_df/', y, '.rds')))
+ }
> 
> sink()
>     
>     
>     
> 
> proc.time()
    user   system  elapsed 
29214.02  1079.80 30648.15 
