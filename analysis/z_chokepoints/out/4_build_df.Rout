
R version 3.6.0 (2019-04-26) -- "Planting of a Tree"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-redhat-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> ##################
> # Compile output #
> ##################
> rm(list = ls())
> 
> wd <- "~/global_energy"
> setwd(wd)
> source(paste0(wd, "/derived/0_globals.R"))
> 
> raw <- "/data/jus3/GlobalIO/raw"
> input <- "/data/jus3/GlobalIO/output/analysis/CP_mat"
> output <- "/data/jus3/GlobalIO/output/analysis/CP_mat/for_tableau"
> temp <- "/data/jus3/GlobalIO/temp"
> figures <- "/data/jus3/GlobalIO/output/figures"
> 
> library('tidyr')
> library('magrittr')

Attaching package: ‘magrittr’

The following object is masked from ‘package:tidyr’:

    extract

> library('dplyr')

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union

> library('hiofunctions')
> library('reshape2')

Attaching package: ‘reshape2’

The following object is masked from ‘package:tidyr’:

    smiths

> 
> sink('~/global_energy/analysis/z_chokepoints/out/4_build_df.txt')
> 
> # Function: import cp matrices
> import_cp_files <- function(strait.name, in.year) {
+   
+   assign('out.name', stringr::str_replace_all(strait.name, "\\.", "_"))
+   
+   assign('Lcp', readRDS(file.path(input, paste0('L_mat/', in.year, '/Lcp_', out.name, '.rds'))), envir = parent.frame())
+   assign('Acp', readRDS(file.path(input, paste0('A_mat/', in.year, '/Acp_', out.name, '.rds'))), envir = parent.frame())
+ }
> 
> # Function: Make matrix long form
> convert_combine <- function(inmat, in.year, exporter_importer) {
+   
+   assign('long_hio', melt(inmat))
+   
+   names(long_hio) <- c('from', 'to', 'value')
+   long_hio$from <- as.character(long_hio$from)
+   long_hio$to <- as.character(long_hio$to)
+   
+   long_hio$from.country <- substr(long_hio$from, 1, 3)
+   long_hio$to.country <- substr(long_hio$to, 1, 3)
+   
+   long_hio$from.sector <- gsub('.*\\.', '', long_hio$from, perl = T)
+   long_hio$to.sector <- gsub('.*\\.', '', long_hio$to, perl = T)
+   
+   #long_hio <- subset(long_hio, to.country != from.country) # Trade only
+   
+   long_hio$year <- in.year
+   
+   # Keep if value grater than 0
+   long_hio <- subset(long_hio, value > 0)
+   
+   if (exporter_importer == 'exporter') {
+     long_hio <- group_by(long_hio, from.country, from.sector, year) %>% summarize(export_value = sum(value))
+   } else if (exporter_importer == 'importer') {
+     long_hio <- group_by(long_hio, to.country, to.sector, year) %>% summarize(import_value = sum(value))
+   }
+   
+   return(long_hio)
+ }
> 
> # Import files
> combine_all <- function(in.year) {
+   
+   environment(import_cp_files) <- environment()
+   
+   assign('L', readRDS(file.path(input, paste0('L_mat/', in.year, '/L', '.rds'))))
+   assign('A', readRDS(file.path(input, paste0('A_mat/', in.year, '/A', '.rds'))))
+   assign('FD', readRDS(file.path(input, paste0('T_mat/', in.year, '/eora_FD.rds'))))
+     FD <- rowSums(FD)
+     FD <- as.data.frame(FD)
+     FD$from <- rownames(FD)
+   
+   assign('A_exports', convert_combine(A, in.year, 'exporter'))
+   assign('A_imports', convert_combine(A, in.year, 'importer'))
+   
+   assign('L_exports', convert_combine(L, in.year, 'exporter'))
+   assign('L_imports', convert_combine(L, in.year, 'importer'))
+   
+   names(A_exports)[1:3] <- names(A_imports)[1:3] <- 
+     names(L_imports)[1:3] <- names(L_exports)[1:3] <- c('country', 'sector', 'year')
+   
+   assign('A_df', full_join(A_exports, A_imports, by = c('country', 'sector', 'year')))
+   assign('L_df', full_join(L_exports, L_imports, by = c('country', 'sector', 'year')))
+   
+   names(A_df) <- c('country', 'sector', 'year', 'A_export_value', 'A_import_value')
+   names(L_df) <- c('country', 'sector', 'year', 'L_export_value', 'L_import_value')
+   
+   assign('alldf', full_join(A_df, L_df, by = c('country', 'sector', 'year')))
+   names(alldf) <- c('country', 'sector', 'year', 'A_export_value_ALL', 'A_import_value_ALL', 'L_export_value_ALL', 'L_import_value_ALL')
+   
+   outdf <- data.frame()
+   for (c in chokepoint_list) {
+     assign('out.name', stringr::str_replace_all(c, "\\.", "_"))
+     
+     if (file.exists(file.path(input, paste0('A_mat/',in.year, '/Acp_', out.name, '.rds')))) {
+       print(paste0('Compiling ', c))
+       import_cp_files(c, in.year)
+       
+       assign('Acp_exports', convert_combine(Acp, in.year, 'exporter'))
+       assign('Acp_imports', convert_combine(Acp, in.year, 'importer'))
+       
+       assign('Lcp_exports', convert_combine(Lcp, in.year, 'exporter'))
+       assign('Lcp_imports', convert_combine(Lcp, in.year, 'importer'))
+       
+       names(Acp_exports)[1:3] <- names(Acp_imports)[1:3] <- 
+         names(Lcp_exports)[1:3] <- names(Lcp_imports)[1:3] <- c('country', 'sector', 'year')
+       
+       assign('Acp_df', full_join(Acp_exports, Acp_imports, by = c('country', 'sector', 'year')))
+       assign('Lcp_df', full_join(Lcp_exports, Lcp_imports, by = c('country', 'sector', 'year')))
+       
+       names(Acp_df) <- c('country', 'sector', 'year', 'A_export_value', 'A_import_value')
+       names(Lcp_df) <- c('country', 'sector', 'year', 'L_export_value', 'L_import_value')
+       
+       assign('cp_df', full_join(Acp_df, Lcp_df, by = c('country', 'sector', 'year')))
+       cp_df$model <- c
+       
+       outdf <- rbind(as.data.frame(outdf), as.data.frame(cp_df))
+     }
+   }
+   alldf <- full_join(alldf, outdf, by = c('country', 'sector', 'year'))
+   
+   return(alldf)
+ }
> 
> outdf <- data.frame()
> for (y in 1995:2015) {
+   print(paste0('#### Compiling t = ', y, ' ####'))
+   indf <- combine_all(y)
+   outdf <- rbind(as.data.frame(outdf), as.data.frame(indf))
+ }
> 
> outdf$share_A_export <- outdf$A_export_value/outdf$A_export_value_ALL
> outdf$share_A_import <- outdf$A_import_value/outdf$A_import_value_ALL
> outdf$share_L_export <- outdf$L_export_value/outdf$L_export_value_ALL
> outdf$share_L_import <- outdf$L_import_value/outdf$L_import_value_ALL
> 
> outdf <- subset(outdf, share_L_export < 1 & share_A_export < 1)
> 
> write.csv(outdf, file.path(output, 'chokepoints.csv'))
> 
> sink()
> 
> proc.time()
    user   system  elapsed 
8272.915  227.006 8505.293 
