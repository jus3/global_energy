
R version 3.5.2 (2018-12-20) -- "Eggshell Igloo"
Copyright (C) 2018 The R Foundation for Statistical Computing
Platform: x86_64-redhat-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> ############################################
> # Compile into dataframe for visualization #
> ############################################
> rm(list = ls())
> 
> wd <- "~/global_energy"
> setwd(wd)
> source(paste0(wd, "/derived/0_globals.R"))
> 
> raw <- "/data/jus3/GlobalIO/raw"
> input <- "/data/jus3/GlobalIO/output/analysis/CP_mat"
> output <- "/data/jus3/GlobalIO/output/analysis/"
> temp <- "/data/jus3/GlobalIO/temp"
> figures <- "/data/jus3/GlobalIO/output/figures"
> 
> library('magrittr')
> library('dplyr')

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union

> library('reshape2')
> library('hiofunctions')
> 
> # Set year of analysis
> in.year <- 2014
> 
> #sink('~/global_energy/analysis/z_chokepoints/out/4_compile_df.txt')
> 
> #############################
> # Import files
> assign('A', readRDS(file.path(input, paste0('A_mat/A_', in.year, '.rds'))))
> 
> 
> # Function: melt matrices
> melt_var <- function(matrix.in) {
+ 
+   assign('mat', get(matrix.in))
+ 
+   mat <- melt(mat)
+   mat$Var1 <- as.character(mat$Var1)
+   mat$Var2 <- as.character(mat$Var2)
+ 
+   mat$from.country <- substr(mat$Var1, 1, 3)
+   mat$to.country <- substr(mat$Var2, 1, 3)
+ 
+   mat$from.sector <- stringr::str_remove(mat$Var1, "^.*\\.")
+   mat$to.sector <- stringr::str_remove(mat$Var2, "^.*\\.")
+ 
+   mat <- mat[c('from.country', 'to.country', 'from.sector', 'to.sector', 'value')]
+ 
+   if (grepl(matrix.in, "A")) {mat$type <- "Direct"} else {mat$type <- "Total"}
+   if (grepl(matrix.in, "cp")) {mat$cp <- 1} else {mat$cp <- 0}
+ 
+   return(mat)
+ }
> 
> # Melt overall
> assign('A.df', melt_var('A'))
> 
> # Melt by chokepoints
> for (c in chokepoint_list) {
+ 
+   print(paste0('##################################'))
+   print(paste0('Compiling STRAIT = ', c))
+ 
+   assign('out.name', stringr::str_replace_all(c, "\\.", "_"))
+   assign('C', readRDS(file.path(input, paste0('C_mat/', out.name, '.rds'))))
+   assert('dim(C) == dim(T)')
+ 
+   if (sum(C) == 0) {
+     print("No chokepoint transits")
+   } else {
+ 
+     assign('Acp', readRDS(file.path(input, paste0('A_mat/Acp_', out.name, '_', in.year, '.rds')))) # Indirect flows
+     assign('Bcp', readRDS(file.path(input, paste0('B_mat/B_', out.name, '_', in.year, '.rds'))))
+ 
+     Acp <- melt_var('Acp')
+     Bcp <- melt_var('Bcp')
+ 
+     Acp$cp <- c
+     Bcp$cp <- c
+ 
+     assign(paste0('Acp.', out.name, '.df'), Acp)
+     assign(paste0('Bcp.', out.name, '.df'), Bcp)
+   }
+ }
[1] "##################################"
[1] "Compiling STRAIT = fury.and.hecla"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = nares"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = belle.isle"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = gibraltar"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = irish.sea"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = kattegat"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = bosporus"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = white.sea"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = bab.el.mandeb"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = hormuz"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = malacca"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = soya.kaikyo"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = panama.canal"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = suez.canal"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = cape.horn"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = cape.of.good.hope"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = danish.straits"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = english.strait"
[1] "Assertion TRUE"
[1] "##################################"
[1] "Compiling STRAIT = east.china.sea"
[1] "Assertion TRUE"
[1] "No chokepoint transits"
[1] "##################################"
[1] "Compiling STRAIT = south.china.sea"
[1] "Assertion TRUE"
> 
> # Combine files (B = direct, A = indirect)
> direct.df <- data.frame(from.country = character(0), to.country = character(0),
+                         from.sector = character(0), to.sector = character(0),
+                         value = numeric(0), type = character(0), cp = character(0))
> indirect.df <- subset(A.df, value > 0)
> 
> 
> for (f in ls(pattern = 'Acp.')) {
+   assign('basedf', get(f))
+   print(paste0('Adding (indirect) STRAIT = ', basedf$cp[1]))
+   basedf <- subset(basedf, value > 0)
+   indirect.df <- rbind(indirect.df, basedf)
+ }
[1] "Adding (indirect) STRAIT = bab.el.mandeb"
[1] "Adding (indirect) STRAIT = bosporus"
[1] "Adding (indirect) STRAIT = danish.straits"
[1] "Adding (indirect) STRAIT = english.strait"
[1] "Adding (indirect) STRAIT = gibraltar"
[1] "Adding (indirect) STRAIT = hormuz"
[1] "Adding (indirect) STRAIT = kattegat"
[1] "Adding (indirect) STRAIT = malacca"
[1] "Adding (indirect) STRAIT = panama.canal"
[1] "Adding (indirect) STRAIT = south.china.sea"
[1] "Adding (indirect) STRAIT = suez.canal"
> 
> for (f in ls(pattern = 'Bcp.')) {
+   assign('basedf', get(f))
+   print(paste0('Adding (direct) STRAIT = ', basedf$cp[1]))
+   basedf <- subset(basedf, value > 0)
+   direct.df <- rbind(direct.df, basedf)
+ }
[1] "Adding (direct) STRAIT = bab.el.mandeb"
[1] "Adding (direct) STRAIT = bosporus"
[1] "Adding (direct) STRAIT = danish.straits"
[1] "Adding (direct) STRAIT = english.strait"
[1] "Adding (direct) STRAIT = gibraltar"
[1] "Adding (direct) STRAIT = hormuz"
[1] "Adding (direct) STRAIT = kattegat"
[1] "Adding (direct) STRAIT = malacca"
[1] "Adding (direct) STRAIT = panama.canal"
[1] "Adding (direct) STRAIT = south.china.sea"
[1] "Adding (direct) STRAIT = suez.canal"
> 
> direct.df$flow <- indirect.df$flow <- 'Monetary'
> indirect.df$flow[indirect.df$from.sector %in% energy.names] <- 'Energy'
> direct.df$flow[direct.df$from.sector %in% energy.names] <- 'Energy'
> 
> # Add regions
> regions <- read.delim(file.path(raw, "ConversionTables/countries_regions.txt"))
> regions <- regions[c('isoAlphaThree', 'unRegionSubregionName')]
> names(regions) <- c('iso.country', 'region.name')
> regions$region.name <- as.character(regions$region.name)
> regions$iso.country <- as.character(regions$iso.country)
> regions <- unique(regions)
> isid('regions', c('iso.country'))
[1] "Dataframe regions is uniquely ID'd by:"
[1] "     iso.country"
> 
> indirect.df <- left_join(indirect.df, regions, by = c('from.country' = 'iso.country'))
> direct.df <- left_join(direct.df, regions, by = c('from.country' = 'iso.country'))
> names(indirect.df)[names(indirect.df) == 'region.name'] <- names(direct.df)[names(direct.df) == 'region.name'] <- 'from.region'
> 
> indirect.df <- left_join(indirect.df, regions, by = c('to.country' = 'iso.country'))
> direct.df <- left_join(direct.df, regions, by = c('to.country' = 'iso.country'))
> names(indirect.df)[names(indirect.df) == 'region.name'] <- names(direct.df)[names(direct.df) == 'region.name'] <- 'to.region'
> 
> # Atach to final demand
> assign('FD', readRDS(file.path(input, paste0('T_mat/eora_FD_', in.year, '.rds'))))
> FD[FD < 0] <- 0
> 
> compile_FD <- function(basedf) {
+   assign('longdf', get(basedf))
+   assign('fd.df', data.frame(FD = numeric(0), to.country = character(0), to.sector = character(0)))
+ 
+   for (c in unique(longdf$to.country)) {
+ 
+     assign('b.fd', FD[grepl(c, rownames(FD)), grepl(c, colnames(FD))])
+ 
+     bv.fd <- rowSums(b.fd)
+ 
+     bdf.fd <- as.data.frame(bv.fd)
+     bdf.fd$to.country <- c
+     bdf.fd$to.sector <- stringr::str_replace(names(bv.fd), paste0(c, "\\."), "")
+ 
+     names(bdf.fd) <- c('FD', 'to.country', 'to.sector')
+ 
+     fd.df <- rbind(fd.df, bdf.fd)
+   }
+ 
+   assign(paste0(basedf, '.FD'), fd.df, envir = parent.frame())
+ }
> add_FD <- function(basedf) {
+   assign('df', get(basedf))
+   assign('df.FD', get(paste0(basedf, '.FD')))
+ 
+   df <- left_join(df, df.FD, by = c('to.sector', 'to.country'))
+ 
+   # Calculate total value
+   df$final.value <- df$value * df$FD
+ 
+   assign(basedf, df, envir = parent.frame())
+ }
> 
> compile_FD('direct.df')
> add_FD('direct.df')
> 
> compile_FD('indirect.df')
> add_FD('indirect.df')
> 
> # Collapse by flow
> from.indirect <- dplyr::group_by(indirect.df, to.country, from.sector, from.region, flow, cp) %>%
+                  dplyr::summarise(indirect.value = sum(final.value))
> from.direct <- dplyr::group_by(direct.df, to.country, from.sector, from.region, flow, cp) %>%
+                dplyr::summarise(direct.value = sum(final.value))
> 
> to.indirect <- dplyr::group_by(indirect.df, to.region, to.sector, from.country, flow, cp) %>%
+                dplyr::summarise(indirect..value = sum(final.value))
> to.direct <- dplyr::group_by(direct.df, to.region, to.sector, from.country, flow, cp) %>%
+              dplyr::summarise(direct.value = sum(final.value))
> 
> # Combine from files and to files
> to.out <- full_join(to.direct, to.indirect, by = c('to.region', 'to.sector', 'from.country',
+                                                    'flow', 'cp'))
> from.out <- full_join(from.direct, from.indirect, by = c('to.country', 'from.sector', 'from.region',
+                                                          'flow', 'cp'))
> 
> # Save files
> write.csv(from.out, file.path(output, paste0("data_viz/chokepoint_FROM_", in.year, ".csv")))
> write.csv(to.out, file.path(output, paste0("data_viz/chokepoint_TO_", in.year, ".csv")))
> 
> 
> sink()
Warning message:
In sink() : no sink to remove
> 
> proc.time()
    user   system  elapsed 
1875.675  161.428 2045.615 
